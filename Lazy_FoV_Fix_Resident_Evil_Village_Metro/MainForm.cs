﻿/*
    This file is part of Lazy FoV Fix.

    Lazy Aspect Fix is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Lazy Aspect Fix is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Lazy Aspect Fix.  If not, see <https://www.gnu.org/licenses/>. 
*/

using System;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms;
using Lazy_FoV_Fix_Resident_Evil_Village.Core;
using Lazy_FoV_Fix_Resident_Evil_Village.Helpers;

namespace Lazy_FoV_Fix_Resident_Evil_Village
{
    public partial class MainUI : MetroFramework.Forms.MetroForm
    {
        private static bool isHotkey;
        private static bool fovIsUnlocked;
        private LazyIni _lazyIni;
        private GlobalKeyboardHook _globalKeyboardHook;
        private KeyEventHandler _keyEventHandler;

        public MainUI()
        {
            InitializeComponent();
            _globalKeyboardHook = new GlobalKeyboardHook();
            _keyEventHandler = new KeyEventHandler(GlobalKeyHook_Down);
            _globalKeyboardHook.HookedKeys.Add(Keys.F1);
            _globalKeyboardHook.KeyDown += _keyEventHandler;
            _globalKeyboardHook.unhook();

            LazyIni.CheckLazyIniUpdated();

            try
            {
                _lazyIni = new LazyIni();
            }
            catch (NoExecutableExecption)
            {
                ClosePatcher();
            }
            catch (Exception ex)
            {
                ErrorLog.DisplayErrorAndLog(ex.ToString(), "ERROR: Failed to initialise Lazy Ini. " + ex, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                ClosePatcher();
            }
        }

        #region Private methods

        private void UpdateZoomFovEnabledState()
        {
            trkZoomFov.Enabled = chkUnlockFov.Checked;
        }

        private void UpdateVignetteEnabledState()
        {
            trkVignette.Enabled = chkVignette.Checked;
        }

        private void UpdateLockedFov()
        {
            var zoomFov = trkBaseFov.Value - 11;
            lblZoomFovValue.Text = (trkZoomFov.Value = (zoomFov < 25) ? 25 : zoomFov).ToString();
            UpdateZoomFovEnabledState();
        }

        private void UpdateFovLabels()
        {
            lblBaseFovValue.Text = $"{trkBaseFov.Value}";
            lblZoomFovValue.Text = $"{trkZoomFov.Value}";
        }

        private float GetRealVignetteFloat(int vignette)
        {
            return (vignette / 100f) * -1f;
        }

        private void ClosePatcher()
        {
            ResidentEvilTools.CloseHandles();
            if (Application.MessageLoop)
            {
                Application.Exit();
            }
            else
            {
                Environment.Exit(1);
            }
        }

        private async void GlobalKeyHook_Down(object sender, KeyEventArgs e)
        {
            if (isHotkey)
            {
                trkBaseFov.Enabled = true;
                trkZoomFov.Enabled = true;
                chkUnlockFov.Enabled = true;
                chkUnlockFovRange.Enabled = true;
                btnReset.Enabled = true;
                lblHotkeyState.Text = "Off";
                lblHotkeyState.ForeColor = Color.Red;
                

                await Task.Run(() => ResidentEvilTools.SetFov(trkBaseFov.Value, trkZoomFov.Value));
                isHotkey = !isHotkey;
            }
            else
            {
                trkBaseFov.Enabled = false;
                trkZoomFov.Enabled = false;
                chkUnlockFov.Enabled = false;
                chkUnlockFovRange.Enabled = false;
                btnReset.Enabled = false;
                lblHotkeyState.Text = "On";
                lblHotkeyState.ForeColor = Color.Green;

                await Task.Run(() => ResidentEvilTools.SetFov(81f, 70f));
                isHotkey = !isHotkey;
            }
        }

        #endregion

        #region UI event handlers

        private async void trkBaseFov_Scroll(object sender, ScrollEventArgs e)
        {
            UpdateFovLabels();

            if (!fovIsUnlocked) { UpdateLockedFov(); }

            _lazyIni.LastBaseFov = trkBaseFov.Value;
            _lazyIni.LastZoomFov = trkZoomFov.Value;
            _lazyIni.SaveLazyIni();

            await Task.Run(() => ResidentEvilTools.SetFov(trkBaseFov.Value, trkZoomFov.Value));
        }

        private async void trkZoomFov_Scroll(object sender, ScrollEventArgs e)
        {
            UpdateFovLabels();

            _lazyIni.LastBaseFov = trkBaseFov.Value;
            _lazyIni.LastZoomFov = trkZoomFov.Value;
            _lazyIni.SaveLazyIni();

            await Task.Run(() => ResidentEvilTools.SetFov(trkBaseFov.Value, trkZoomFov.Value));
        }

        private async void chkUnlockFov_CheckedChanged(object sender, EventArgs e)
        {
            if (!chkUnlockFov.Checked) { UpdateLockedFov(); }

            fovIsUnlocked = chkUnlockFov.Checked;
            UpdateZoomFovEnabledState();

            _lazyIni.LastUnlockedState = chkUnlockFov.Checked;
            _lazyIni.SaveLazyIni();

            await Task.Run(() => ResidentEvilTools.SetFov(trkBaseFov.Value, trkZoomFov.Value));
        }

        private async void btnReset_Click(object sender, EventArgs e)
        {
            trkBaseFov.Value = 81;
            trkZoomFov.Value = 70;
            UpdateFovLabels();
            await Task.Run(() => ResidentEvilTools.SetFov(trkBaseFov.Value, trkZoomFov.Value));
        }

        private async void MainForm_Load(object sender, EventArgs e)
        {
            lazyProgressSpinner.Spinning = true;
            lazyProgressSpinner.Visible = true;
            trkBaseFov.Enabled = false;
            trkZoomFov.Enabled = false;
            chkUnlockFov.Enabled = false;
            chkUnlockFovRange.Enabled = false;
            chkVignette.Enabled = false;
            btnReset.Enabled = false;

            lblLoading.Text = "Waiting for game process...";
            try
            {
                await Task.Run(() => ResidentEvilTools.GetProcessHandle(_lazyIni.ExecutableName.Replace(".exe", "")));
            }
            catch (Exception ex)
            {
                ErrorLog.DisplayErrorAndLog(ex.Message, "ERROR | Failed to get process handle.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            lblLoading.Text = "Finidng initial values...";
            try
            {
                await Task.Run(() =>
                    ResidentEvilTools.SetTargetAddresses(
                        _lazyIni.AddressRangeStartBaseFov,
                        _lazyIni.AddressRangeEndBaseFov,
                        _lazyIni.AddressRangeStartZoomFov,
                        _lazyIni.AddressRangeEndZoomFov,
                        _lazyIni.AddressRangeStartVignette,
                        _lazyIni.AddressRangeEndVignette
                    )
                );
            }
            catch (Exception ex)
            {
                ErrorLog.DisplayErrorAndLog(ex.ToString(), "ERROR | Failed to set target addresses", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            _lazyIni.AddressRangeStartBaseFov = ((ResidentEvilTools.BaseFovAddress / 10) > 0) ? ResidentEvilTools.BaseFovAddress / 10 : 0;
            _lazyIni.AddressRangeEndBaseFov = ((ResidentEvilTools.BaseFovAddress * 10) < ulong.MaxValue) ? ResidentEvilTools.BaseFovAddress * 10 : ulong.MaxValue;
            _lazyIni.AddressRangeStartZoomFov = ((ResidentEvilTools.ZoomFovAddress / 10) > 0) ? ResidentEvilTools.ZoomFovAddress / 10 : 0;
            _lazyIni.AddressRangeEndZoomFov = ((ResidentEvilTools.ZoomFovAddress * 10) < ulong.MaxValue) ? ResidentEvilTools.ZoomFovAddress * 10 : ulong.MaxValue;
            _lazyIni.AddressRangeStartVignette = ((ResidentEvilTools.VignetteCodeAddress / 10) > 0) ? ResidentEvilTools.VignetteCodeAddress / 10 : 0;
            _lazyIni.AddressRangeEndVignette = ((ResidentEvilTools.VignetteCodeAddress * 10) < ulong.MaxValue) ? ResidentEvilTools.VignetteCodeAddress * 10 : ulong.MaxValue;
            _lazyIni.SaveLazyIni();

            trkBaseFov.Value = _lazyIni.LastBaseFov;
            trkZoomFov.Value = _lazyIni.LastZoomFov;
            chkUnlockFov.Checked = _lazyIni.LastUnlockedState;
            chkUnlockFovRange.Checked = _lazyIni.LastUnlockedRangeState;
            chkVignette.Checked = _lazyIni.LastVignetteState;
            trkVignette.Value = _lazyIni.LastVignette;

            fovIsUnlocked = chkUnlockFov.Checked;

            UpdateZoomFovEnabledState();
            UpdateVignetteEnabledState();
            UpdateFovLabels();

            lblLoading.Text = null;
            UpdateZoomFovEnabledState();
            UpdateVignetteEnabledState();
            trkBaseFov.Enabled = true;
            chkVignette.Enabled = true;
            chkUnlockFovRange.Enabled = true;
            chkUnlockFov.Enabled = true;
            btnReset.Enabled = true;
            lazyProgressSpinner.Spinning = false;
            lazyProgressSpinner.Visible = false;
            await Task.Run(() => ResidentEvilTools.SetFov(trkBaseFov.Value, trkZoomFov.Value));
            await Task.Run(() => ResidentEvilTools.SetVignette(GetRealVignetteFloat(trkVignette.Value)));

            _globalKeyboardHook.hook();
        }

        private async void chkUnlockFovRange_CheckedChanged(object sender, EventArgs e)
        {
            var baseFov = trkBaseFov.Value;
            var zoomFov = trkZoomFov.Value;

            if (chkUnlockFovRange.Checked)
            {
                trkBaseFov.Maximum = 200;
                trkZoomFov.Maximum = 200;

                _lazyIni.LastUnlockedRangeState = chkUnlockFovRange.Checked;
                _lazyIni.SaveLazyIni();

                return;
            }
            trkBaseFov.Maximum = 150;
            trkZoomFov.Maximum = 150;

            _lazyIni.LastUnlockedRangeState = chkUnlockFovRange.Checked;
            _lazyIni.SaveLazyIni();

            if (baseFov > 150 || zoomFov > 150)
            {
                trkZoomFov.Value = trkBaseFov.Value - 11;
                await Task.Run(() => ResidentEvilTools.SetFov(trkBaseFov.Value, trkZoomFov.Value));
            }
        }

        private async void chkVignette_CheckedChanged(object sender, EventArgs e)
        {
            _lazyIni.LastVignetteState = chkVignette.Checked;
            _lazyIni.SaveLazyIni();

            UpdateVignetteEnabledState();

            if (chkVignette.Checked)
            {
                await Task.Run(() => ResidentEvilTools.EnableVignetteOverride());
                return;
            }
            await Task.Run(() => ResidentEvilTools.DisableVignetteOverride());
        }

        private async void trkVignette_Scroll(object sender, ScrollEventArgs e)
        {
            _lazyIni.LastVignette = trkVignette.Value;
            _lazyIni.SaveLazyIni();
            await Task.Run(() => ResidentEvilTools.SetVignette(GetRealVignetteFloat(trkVignette.Value)));
        }

        #endregion
    }
}

﻿using System;
using System.IO;
using System.Windows.Forms;

namespace Lazy_FoV_Fix_Resident_Evil_Village.Helpers
{
    class ErrorLog
    {
        private static void Log(string error, string stack)
        {
            var logFile = File.AppendText("./lazy_error.log");

            logFile.WriteLine(DateTime.Now + " | " + error + " | Stack trace: " + stack);
            logFile.Flush();
            logFile.Close();
            logFile.Dispose();
        }

        public static void DisplayErrorAndLog(string ex, string messageBoxText, string messageBoxTitle, MessageBoxButtons buttons, MessageBoxIcon icon)
        {
            MessageBox.Show(messageBoxText, messageBoxTitle, buttons, icon);
            Log(ex, Environment.StackTrace);
        }
    }
}

﻿using System;
using System.IO;
using System.Windows.Forms;
using MadMilkman.Ini;
using Lazy_FoV_Fix_Resident_Evil_Village.Helpers;

namespace Lazy_FoV_Fix_Resident_Evil_Village.Core
{
    public class LazyIni
    {
        public int IniVer { get; }
        public int LastBaseFov { get; set; }
        public int LastZoomFov { get; set; }
        public bool LastUnlockedState { get; set; }
        public bool LastUnlockedRangeState { get; set; }
        public string ExecutableName { get; set; }
        public ulong AddressRangeStartBaseFov { get; set; }
        public ulong AddressRangeEndBaseFov { get; set; }
        public ulong AddressRangeStartZoomFov { get; set; }
        public ulong AddressRangeEndZoomFov { get; set; }
        public ulong AddressRangeStartVignette { get; set; }
        public ulong AddressRangeEndVignette { get; set; }
        public bool LastVignetteState { get; set; }
        public int LastVignette { get; set; }

        public LazyIni(string path = "./lazy.ini")
        {
            if (!File.Exists(path))
            {
                CreateLazyIni();
            }
            var lazyIni = LoadIni(path);

            IniVer = Convert.ToInt32(lazyIni.Sections["Version"].Keys["IniVersion"].Value);
            LastBaseFov = Convert.ToInt32(lazyIni.Sections["FoV"].Keys["lastBaseFov"].Value);
            LastZoomFov = Convert.ToInt32(lazyIni.Sections["FoV"].Keys["lastZoomFov"].Value);
            LastUnlockedState = Convert.ToBoolean(lazyIni.Sections["Settings"].Keys["lastUnlockedState"].Value);
            LastUnlockedRangeState = Convert.ToBoolean(lazyIni.Sections["Settings"].Keys["lastUnlockedRangeState"].Value);
            ExecutableName = Convert.ToString(lazyIni.Sections["Settings"].Keys["executableName"].Value);
            AddressRangeStartBaseFov = Convert.ToUInt64(lazyIni.Sections["Settings"].Keys["addressRangeStartBaseFov"].Value);
            AddressRangeEndBaseFov = Convert.ToUInt64(lazyIni.Sections["Settings"].Keys["addressRangeEndBaseFov"].Value);
            AddressRangeStartZoomFov = Convert.ToUInt64(lazyIni.Sections["Settings"].Keys["addressRangeStartZoomFov"].Value);
            AddressRangeEndZoomFov = Convert.ToUInt64(lazyIni.Sections["Settings"].Keys["addressRangeEndZoomFov"].Value);
            AddressRangeStartVignette = Convert.ToUInt64(lazyIni.Sections["Settings"].Keys["addressRangeStartVignette"].Value);
            AddressRangeEndVignette = Convert.ToUInt64(lazyIni.Sections["Settings"].Keys["addressRangeEndVignette"].Value);
            LastVignetteState = Convert.ToBoolean(lazyIni.Sections["Settings"].Keys["lastVignetteState"].Value);
            LastVignette = Convert.ToInt32(lazyIni.Sections["Settings"].Keys["lastVignette"].Value);
        }

        public string getGameExeLoc()
        {
            System.Windows.Forms.MessageBox.Show("Please navigate to Resident Evil Village executable.", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information);

            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.InitialDirectory = "C:\\";
                var response = openFileDialog.ShowDialog();

                if (response == DialogResult.Cancel || response == DialogResult.Abort)
                {
                    throw new NoExecutableExecption("User aborted.");
                }
                else if (response == DialogResult.OK)
                {

                    return Path.GetFileNameWithoutExtension(openFileDialog.FileName);
                }
                else
                {
                    return getGameExeLoc();
                }
            }
        }

        public void CreateLazyIni()
        {
            var lazyIni = new IniFile();
            IniSection iniVersion = lazyIni.Sections.Add("Version");
            IniSection iniFov = lazyIni.Sections.Add("FoV");
            IniSection iniSettings = lazyIni.Sections.Add("Settings");
            iniVersion.Keys.Add("IniVersion", "4");
            iniFov.Keys.Add("lastBaseFov", "81");
            iniFov.Keys.Add("lastZoomFov", "70");
            iniSettings.Keys.Add("lastUnlockedState", "false");
            iniSettings.Keys.Add("lastUnlockedRangeState", "false");
            iniSettings.Keys.Add("executableName", Path.GetFileName(getGameExeLoc()));
            iniSettings.Keys.Add("addressRangeStartBaseFov", $"{0UL}");
            iniSettings.Keys.Add("addressRangeEndBaseFov", $"{0UL}");
            iniSettings.Keys.Add("addressRangeStartZoomFov", $"{0UL}");
            iniSettings.Keys.Add("addressRangeEndZoomFov", $"{0UL}");
            iniSettings.Keys.Add("addressRangeStartVignette", $"{0UL}");
            iniSettings.Keys.Add("addressRangeEndVignette", $"{0UL}");
            iniSettings.Keys.Add("lastVignetteState", "false");
            iniSettings.Keys.Add("lastVignette", $"{0}");

            lazyIni.Save("./lazy.ini");
        }

        public void SaveLazyIni()
        {
            var lazyIni = LoadIni("./lazy.ini");

            lazyIni.Sections["Version"].Keys["iniVersion"].Value = $"{IniVer}";
            lazyIni.Sections["FoV"].Keys["lastBaseFov"].Value = $"{LastBaseFov}";
            lazyIni.Sections["FoV"].Keys["lastZoomFov"].Value = $"{LastZoomFov}";
            lazyIni.Sections["Settings"].Keys["lastUnlockedState"].Value = $"{LastUnlockedState}";
            lazyIni.Sections["Settings"].Keys["lastUnlockedRangeState"].Value = $"{LastUnlockedRangeState}";
            lazyIni.Sections["Settings"].Keys["executableName"].Value = $"{ExecutableName}";
            lazyIni.Sections["Settings"].Keys["addressRangeStartBaseFov"].Value = $"{AddressRangeStartBaseFov}";
            lazyIni.Sections["Settings"].Keys["addressRangeEndBaseFov"].Value = $"{AddressRangeEndBaseFov}";
            lazyIni.Sections["Settings"].Keys["addressRangeStartZoomFov"].Value = $"{AddressRangeStartBaseFov}";
            lazyIni.Sections["Settings"].Keys["addressRangeEndZoomFov"].Value = $"{AddressRangeEndBaseFov}";
            lazyIni.Sections["Settings"].Keys["addressRangeStartVignette"].Value = $"{AddressRangeStartVignette}";
            lazyIni.Sections["Settings"].Keys["addressRangeEndVignette"].Value = $"{AddressRangeEndVignette}";
            lazyIni.Sections["Settings"].Keys["lastVignetteState"].Value = $"{LastVignetteState}";
            lazyIni.Sections["Settings"].Keys["lastVignette"].Value = $"{LastVignette}";

            bool isInUse = true;
            while (isInUse)
            {
                try
                {
                    using (FileStream stream = new FileInfo("./lazy.ini").Open(FileMode.Open, FileAccess.Read, FileShare.None))
                    {
                        stream.Close();
                        lazyIni.Save("./lazy.ini");
                        isInUse = false;
                    }
                }
                catch (IOException)
                {
                    continue;
                }
            }
        }

        public static IniFile LoadIni(string path)
        {
            if (!File.Exists(path))
            {
                ErrorLog.DisplayErrorAndLog("File not found: " + path, "ERROR: Could not find " + path, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                throw new FileNotFoundException();
            }

            var iniFile = new IniFile();
            iniFile.Load(path);
            return iniFile;
        }

        public static void CheckLazyIniUpdated()
        {
            if (File.Exists("./lazy.ini"))
            {
                var lazyIni = new IniFile();
                lazyIni.Load("./lazy.ini");
                if (lazyIni.Sections["Version"] == null ||
                    lazyIni.Sections["Version"].Keys["iniVersion"] == null ||
                    lazyIni.Sections["Version"].Keys["iniVersion"].Value != "4")
                {
                    File.Delete("./lazy.ini");
                }
            }
        }
    }

    [Serializable]
    public class NoExecutableExecption : Exception
    {
        public NoExecutableExecption(string message) : base(message)
        {
        }

        public NoExecutableExecption(string message, Exception innerException) : base(message, innerException)
        {
        }

        public NoExecutableExecption()
        {
        }

        protected NoExecutableExecption(System.Runtime.Serialization.SerializationInfo serializationInfo, System.Runtime.Serialization.StreamingContext streamingContext)
        {
            throw new NotImplementedException();
        }
    }
}

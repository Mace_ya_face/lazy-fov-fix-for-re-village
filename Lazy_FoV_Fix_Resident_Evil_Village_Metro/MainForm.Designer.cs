﻿/*
    This file is part of Lazy Aspect Fix.

    Foobar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Lazy Aspect Fix is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Lazy Aspect Fix.  If not, see <https://www.gnu.org/licenses/>. 
*/

namespace Lazy_FoV_Fix_Resident_Evil_Village
{
    partial class MainUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainUI));
            this.lazyProgressSpinner = new MetroFramework.Controls.MetroProgressSpinner();
            this.lazyOutput = new MetroFramework.Controls.MetroLabel();
            this.lblBaseFov = new MetroFramework.Controls.MetroLabel();
            this.lblZoomFov = new MetroFramework.Controls.MetroLabel();
            this.chkUnlockFov = new MetroFramework.Controls.MetroCheckBox();
            this.trkZoomFov = new MetroFramework.Controls.MetroTrackBar();
            this.trkBaseFov = new MetroFramework.Controls.MetroTrackBar();
            this.lblZoomFovValue = new MetroFramework.Controls.MetroLabel();
            this.lblBaseFovValue = new MetroFramework.Controls.MetroLabel();
            this.metroStyleManager1 = new MetroFramework.Components.MetroStyleManager(this.components);
            this.btnReset = new MetroFramework.Controls.MetroButton();
            this.lblLoading = new MetroFramework.Controls.MetroLabel();
            this.chkUnlockFovRange = new MetroFramework.Controls.MetroCheckBox();
            this.chkVignette = new MetroFramework.Controls.MetroCheckBox();
            this.lblVignetteTrk = new MetroFramework.Controls.MetroLabel();
            this.trkVignette = new MetroFramework.Controls.MetroTrackBar();
            this.lblHotKey = new MetroFramework.Controls.MetroLabel();
            this.lblHotkeyState = new MetroFramework.Controls.MetroLabel();
            ((System.ComponentModel.ISupportInitialize)(this.metroStyleManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // lazyProgressSpinner
            // 
            this.lazyProgressSpinner.Location = new System.Drawing.Point(467, 275);
            this.lazyProgressSpinner.Maximum = 100;
            this.lazyProgressSpinner.Name = "lazyProgressSpinner";
            this.lazyProgressSpinner.Size = new System.Drawing.Size(40, 39);
            this.lazyProgressSpinner.Style = MetroFramework.MetroColorStyle.Purple;
            this.lazyProgressSpinner.TabIndex = 5;
            this.lazyProgressSpinner.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.lazyProgressSpinner.UseSelectable = true;
            this.lazyProgressSpinner.Visible = false;
            // 
            // lazyOutput
            // 
            this.lazyOutput.AutoSize = true;
            this.lazyOutput.Location = new System.Drawing.Point(41, 172);
            this.lazyOutput.Name = "lazyOutput";
            this.lazyOutput.Size = new System.Drawing.Size(0, 0);
            this.lazyOutput.TabIndex = 6;
            this.lazyOutput.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // lblBaseFov
            // 
            this.lblBaseFov.AutoSize = true;
            this.lblBaseFov.BackColor = System.Drawing.Color.Transparent;
            this.lblBaseFov.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblBaseFov.Location = new System.Drawing.Point(20, 68);
            this.lblBaseFov.Name = "lblBaseFov";
            this.lblBaseFov.Size = new System.Drawing.Size(64, 19);
            this.lblBaseFov.TabIndex = 7;
            this.lblBaseFov.Text = "Base Fov:";
            this.lblBaseFov.UseCustomBackColor = true;
            this.lblBaseFov.UseCustomForeColor = true;
            // 
            // lblZoomFov
            // 
            this.lblZoomFov.AutoSize = true;
            this.lblZoomFov.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblZoomFov.Location = new System.Drawing.Point(9, 97);
            this.lblZoomFov.Name = "lblZoomFov";
            this.lblZoomFov.Size = new System.Drawing.Size(75, 19);
            this.lblZoomFov.TabIndex = 8;
            this.lblZoomFov.Text = "Zoom FoV:";
            this.lblZoomFov.UseCustomBackColor = true;
            this.lblZoomFov.UseCustomForeColor = true;
            // 
            // chkUnlockFov
            // 
            this.chkUnlockFov.AutoSize = true;
            this.chkUnlockFov.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.chkUnlockFov.Location = new System.Drawing.Point(9, 172);
            this.chkUnlockFov.Name = "chkUnlockFov";
            this.chkUnlockFov.Size = new System.Drawing.Size(170, 15);
            this.chkUnlockFov.TabIndex = 9;
            this.chkUnlockFov.Text = "Unlock Base And Zoom FoV";
            this.chkUnlockFov.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.chkUnlockFov.UseCustomBackColor = true;
            this.chkUnlockFov.UseCustomForeColor = true;
            this.chkUnlockFov.UseSelectable = true;
            this.chkUnlockFov.CheckedChanged += new System.EventHandler(this.chkUnlockFov_CheckedChanged);
            // 
            // trkZoomFov
            // 
            this.trkZoomFov.BackColor = System.Drawing.Color.Transparent;
            this.trkZoomFov.Enabled = false;
            this.trkZoomFov.Location = new System.Drawing.Point(96, 97);
            this.trkZoomFov.Maximum = 150;
            this.trkZoomFov.Minimum = 25;
            this.trkZoomFov.Name = "trkZoomFov";
            this.trkZoomFov.Size = new System.Drawing.Size(411, 23);
            this.trkZoomFov.TabIndex = 10;
            this.trkZoomFov.Text = "Zoom FoV";
            this.trkZoomFov.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.trkZoomFov.UseCustomBackColor = true;
            this.trkZoomFov.Value = 70;
            this.trkZoomFov.Scroll += new System.Windows.Forms.ScrollEventHandler(this.trkZoomFov_Scroll);
            // 
            // trkBaseFov
            // 
            this.trkBaseFov.BackColor = System.Drawing.Color.Transparent;
            this.trkBaseFov.Location = new System.Drawing.Point(96, 68);
            this.trkBaseFov.Maximum = 150;
            this.trkBaseFov.Minimum = 25;
            this.trkBaseFov.Name = "trkBaseFov";
            this.trkBaseFov.Size = new System.Drawing.Size(411, 23);
            this.trkBaseFov.TabIndex = 11;
            this.trkBaseFov.Text = "Base FoV";
            this.trkBaseFov.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.trkBaseFov.UseCustomBackColor = true;
            this.trkBaseFov.Value = 81;
            this.trkBaseFov.Scroll += new System.Windows.Forms.ScrollEventHandler(this.trkBaseFov_Scroll);
            // 
            // lblZoomFovValue
            // 
            this.lblZoomFovValue.AutoSize = true;
            this.lblZoomFovValue.Location = new System.Drawing.Point(516, 99);
            this.lblZoomFovValue.Name = "lblZoomFovValue";
            this.lblZoomFovValue.Size = new System.Drawing.Size(0, 0);
            this.lblZoomFovValue.TabIndex = 12;
            // 
            // lblBaseFovValue
            // 
            this.lblBaseFovValue.AutoSize = true;
            this.lblBaseFovValue.Location = new System.Drawing.Point(516, 69);
            this.lblBaseFovValue.Name = "lblBaseFovValue";
            this.lblBaseFovValue.Size = new System.Drawing.Size(0, 0);
            this.lblBaseFovValue.TabIndex = 13;
            // 
            // metroStyleManager1
            // 
            this.metroStyleManager1.Owner = null;
            this.metroStyleManager1.Style = MetroFramework.MetroColorStyle.Purple;
            this.metroStyleManager1.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // btnReset
            // 
            this.btnReset.Location = new System.Drawing.Point(9, 291);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(75, 23);
            this.btnReset.TabIndex = 14;
            this.btnReset.Text = "Reset";
            this.btnReset.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.btnReset.UseSelectable = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // lblLoading
            // 
            this.lblLoading.AutoSize = true;
            this.lblLoading.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblLoading.Location = new System.Drawing.Point(283, 296);
            this.lblLoading.Name = "lblLoading";
            this.lblLoading.Size = new System.Drawing.Size(0, 0);
            this.lblLoading.TabIndex = 15;
            this.lblLoading.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.lblLoading.UseCustomBackColor = true;
            this.lblLoading.UseCustomForeColor = true;
            // 
            // chkUnlockFovRange
            // 
            this.chkUnlockFovRange.AutoSize = true;
            this.chkUnlockFovRange.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.chkUnlockFovRange.Location = new System.Drawing.Point(9, 193);
            this.chkUnlockFovRange.Name = "chkUnlockFovRange";
            this.chkUnlockFovRange.Size = new System.Drawing.Size(383, 30);
            this.chkUnlockFovRange.TabIndex = 16;
            this.chkUnlockFovRange.Text = "Unlock FoV Range\r\n[WARNING] The game can start to get upset at FoV values above 1" +
    "50";
            this.chkUnlockFovRange.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.chkUnlockFovRange.UseCustomBackColor = true;
            this.chkUnlockFovRange.UseCustomForeColor = true;
            this.chkUnlockFovRange.UseSelectable = true;
            this.chkUnlockFovRange.CheckedChanged += new System.EventHandler(this.chkUnlockFovRange_CheckedChanged);
            // 
            // chkVignette
            // 
            this.chkVignette.AutoSize = true;
            this.chkVignette.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.chkVignette.Location = new System.Drawing.Point(9, 230);
            this.chkVignette.Name = "chkVignette";
            this.chkVignette.Size = new System.Drawing.Size(115, 15);
            this.chkVignette.TabIndex = 17;
            this.chkVignette.Text = "Override Vignette";
            this.chkVignette.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.chkVignette.UseCustomBackColor = true;
            this.chkVignette.UseCustomForeColor = true;
            this.chkVignette.UseSelectable = true;
            this.chkVignette.CheckedChanged += new System.EventHandler(this.chkVignette_CheckedChanged);
            // 
            // lblVignetteTrk
            // 
            this.lblVignetteTrk.AutoSize = true;
            this.lblVignetteTrk.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblVignetteTrk.Location = new System.Drawing.Point(20, 123);
            this.lblVignetteTrk.Name = "lblVignetteTrk";
            this.lblVignetteTrk.Size = new System.Drawing.Size(60, 19);
            this.lblVignetteTrk.TabIndex = 18;
            this.lblVignetteTrk.Text = "Vignette:";
            this.lblVignetteTrk.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.lblVignetteTrk.UseCustomBackColor = true;
            this.lblVignetteTrk.UseCustomForeColor = true;
            // 
            // trkVignette
            // 
            this.trkVignette.BackColor = System.Drawing.Color.Transparent;
            this.trkVignette.Location = new System.Drawing.Point(96, 123);
            this.trkVignette.Minimum = -100;
            this.trkVignette.Name = "trkVignette";
            this.trkVignette.Size = new System.Drawing.Size(411, 23);
            this.trkVignette.TabIndex = 19;
            this.trkVignette.Text = "metroTrackBar1";
            this.trkVignette.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.trkVignette.Value = 0;
            this.trkVignette.Scroll += new System.Windows.Forms.ScrollEventHandler(this.trkVignette_Scroll);
            // 
            // lblHotKey
            // 
            this.lblHotKey.AutoSize = true;
            this.lblHotKey.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblHotKey.Location = new System.Drawing.Point(9, 252);
            this.lblHotKey.Name = "lblHotKey";
            this.lblHotKey.Size = new System.Drawing.Size(152, 19);
            this.lblHotKey.TabIndex = 20;
            this.lblHotKey.Text = "Default FoV Hotkey (F1): ";
            this.lblHotKey.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.lblHotKey.UseCustomBackColor = true;
            this.lblHotKey.UseCustomForeColor = true;
            // 
            // lblHotkeyState
            // 
            this.lblHotkeyState.AutoSize = true;
            this.lblHotkeyState.ForeColor = System.Drawing.Color.Red;
            this.lblHotkeyState.Location = new System.Drawing.Point(154, 253);
            this.lblHotkeyState.Name = "lblHotkeyState";
            this.lblHotkeyState.Size = new System.Drawing.Size(28, 19);
            this.lblHotkeyState.TabIndex = 21;
            this.lblHotkeyState.Text = "Off";
            this.lblHotkeyState.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.lblHotkeyState.UseCustomBackColor = true;
            this.lblHotkeyState.UseCustomForeColor = true;
            // 
            // MainUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(555, 325);
            this.Controls.Add(this.lblHotkeyState);
            this.Controls.Add(this.lblHotKey);
            this.Controls.Add(this.trkVignette);
            this.Controls.Add(this.lblVignetteTrk);
            this.Controls.Add(this.chkVignette);
            this.Controls.Add(this.chkUnlockFovRange);
            this.Controls.Add(this.lblLoading);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.lblBaseFovValue);
            this.Controls.Add(this.lblZoomFovValue);
            this.Controls.Add(this.trkBaseFov);
            this.Controls.Add(this.trkZoomFov);
            this.Controls.Add(this.chkUnlockFov);
            this.Controls.Add(this.lblZoomFov);
            this.Controls.Add(this.lblBaseFov);
            this.Controls.Add(this.lazyOutput);
            this.Controls.Add(this.lazyProgressSpinner);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainUI";
            this.Resizable = false;
            this.Style = MetroFramework.MetroColorStyle.Purple;
            this.Text = "Lazy FoV And Vignette Fix: Resident Evil: Village";
            this.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.metroStyleManager1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private MetroFramework.Controls.MetroProgressSpinner lazyProgressSpinner;
        private MetroFramework.Controls.MetroLabel lazyOutput;
        private MetroFramework.Controls.MetroLabel lblBaseFov;
        private MetroFramework.Controls.MetroLabel lblZoomFov;
        private MetroFramework.Controls.MetroCheckBox chkUnlockFov;
        private MetroFramework.Controls.MetroTrackBar trkZoomFov;
        private MetroFramework.Controls.MetroTrackBar trkBaseFov;
        private MetroFramework.Controls.MetroLabel lblZoomFovValue;
        private MetroFramework.Controls.MetroLabel lblBaseFovValue;
        private MetroFramework.Components.MetroStyleManager metroStyleManager1;
        private MetroFramework.Controls.MetroButton btnReset;
        private MetroFramework.Controls.MetroLabel lblLoading;
        private MetroFramework.Controls.MetroCheckBox chkUnlockFovRange;
        private MetroFramework.Controls.MetroCheckBox chkVignette;
        private MetroFramework.Controls.MetroLabel lblVignetteTrk;
        private MetroFramework.Controls.MetroTrackBar trkVignette;
        private MetroFramework.Controls.MetroLabel lblHotKey;
        private MetroFramework.Controls.MetroLabel lblHotkeyState;
    }
}


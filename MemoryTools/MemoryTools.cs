﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace MemoryTools
{
    public static class MemoryTools
    {
        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern int VirtualQueryEx(IntPtr hProcess, IntPtr lpAddress, out WindowsMemoryHelper.MEMORY_BASIC_INFORMATION64 lpBuffer, uint dwLength);
        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern bool ReadProcessMemory(IntPtr hProcess, IntPtr lpBaseAddress, [Out] byte[] lpBuffer, int dwSize, out int lpNumberOfBytesRead);
        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern bool WriteProcessMemory(IntPtr hProcess, ulong lpBaseAddress, byte[] lpBuffer, int dwSize, out int lpNumberOfBytesWritten);
        [DllImport("kernel32.dll")]
        private static extern void GetSystemInfo(out SYSTEM_INFO lpSystemInfo);

        public struct SYSTEM_INFO
        {
            public ushort processorArchitecture;
            ushort reserved;
            public uint pageSize;
            public IntPtr minimumApplicationAddress;
            public IntPtr maximumApplicationAddress;
            public IntPtr activeProcessorMask;
            public uint numberOfProcessors;
            public uint processorType;
            public uint allocationGranularity;
            public ushort processorLevel;
            public ushort processorRevision;
        }

        public static ulong GetAddressByteArray(Process targetProcess, BytePattern searchTarget, ulong startRange, ulong endRange)
        {
            if (endRange == 0)
            {
                SYSTEM_INFO sys_info = new SYSTEM_INFO();
                GetSystemInfo(out sys_info);
                endRange = (ulong)sys_info.maximumApplicationAddress;
            }
            if (startRange == 0)
            {
                SYSTEM_INFO sys_info = new SYSTEM_INFO();
                GetSystemInfo(out sys_info);
                startRange = (ulong)sys_info.minimumApplicationAddress;
            }
            ulong currentPointer = startRange;
            int bytesRead;
            ulong result = 0;

            while (currentPointer < endRange && result == 0)
            {
                WindowsMemoryHelper.MEMORY_BASIC_INFORMATION64 memoryRegion;

                var structByteCount = VirtualQueryEx(targetProcess.Handle, (IntPtr)currentPointer, out memoryRegion, (uint)Marshal.SizeOf(typeof(WindowsMemoryHelper.MEMORY_BASIC_INFORMATION64)));

                if (structByteCount > 0
                    && memoryRegion.RegionSize > 0
                    && memoryRegion.State == (uint)WindowsMemoryHelper.RegionPageState.MEM_COMMIT
                    && WindowsMemoryHelper.CheckProtection(searchTarget, memoryRegion.Protect))
                {
                    var regionStartAddress = memoryRegion.BaseAddress;
                    if (startRange > regionStartAddress)
                    {
                        regionStartAddress = startRange;
                    }

                    var regionEndAddress = memoryRegion.BaseAddress + memoryRegion.RegionSize;
                    if (endRange < regionEndAddress)
                    {
                        regionEndAddress = endRange;
                    }

                    ulong regionBytesToRead = regionEndAddress - regionStartAddress;
                    byte[] regionBytes = new byte[regionBytesToRead];

                    ReadProcessMemory(targetProcess.Handle, (IntPtr)regionStartAddress, regionBytes, regionBytes.Length, out bytesRead);

                    var locateResult = Locate(regionBytes, searchTarget.Bytes);

                    if (locateResult != -1)
                    {
                        result = regionStartAddress + (ulong)locateResult;
                    }
                }
                if (memoryRegion.RegionSize != 0)
                {
                    currentPointer = memoryRegion.BaseAddress + memoryRegion.RegionSize;
                }
                else
                {
                    currentPointer += 1;
                }
            }

            return result;
        }

        public static void WriteSingleArray(Process targetProcess, ulong baseAddress, byte[] newArray)
        {
            int bytesWritten;
            if (targetProcess != null)
            {
                if (!WriteProcessMemory(targetProcess.Handle, baseAddress, newArray, newArray.Length, out bytesWritten))
                {
                    throw new Exception("Failed to write new array. Address: " + baseAddress);
                }
            }
        }

        public static int Locate(byte[] self, byte[] candidate)
        {
            if (IsEmptyLocate(self, candidate))
                return -1;

            for (int i = 0; i < self.Length; i++)
            {
                if (!IsMatch(self, i, candidate))
                    continue;

                return (i);
            }

            return -1;
        }

        private static bool IsMatch(byte[] array, int position, byte[] candidate)
        {
            if (candidate.Length > (array.Length - position))
                return false;

            for (int i = 0; i < candidate.Length; i++)
            {
                if (array[position + i] != candidate[i])
                    return false;
            }

            return true;
        }

        private static bool IsEmptyLocate(byte[] array, byte[] candidate)
        {
            return array == null
                || candidate == null
                || array.Length == 0
                || candidate.Length == 0
                || candidate.Length > array.Length;
        }
    }
}

﻿using System.Collections.Generic;

namespace MemoryTools
{
    public class BytePattern
    {
        public BytePatternConfig Config { get; }
        public byte[] Bytes { get; private set; }
        public List<ulong> MatchedAddresses { get; private set; }
        public AddressRange AddressRange { get; private set; }

        public BytePattern(BytePatternConfig config)
        {
            Config = config;
            Bytes = config.Bytes;
            MatchedAddresses = new List<ulong>();
            AddressRange = new AddressRange(config.AddressRangeStart, config.AddressRangeEnd);
        }
    }
}

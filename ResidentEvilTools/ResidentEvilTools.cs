﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Windows.Forms;
using MemoryTools;

namespace Lazy_FoV_Fix_Resident_Evil_Village.Core
{
    public static class ResidentEvilTools
    {
        public static readonly byte[] VignetteSetCode = { 0x48, 0x8B, 0x41, 0x30, 0xF3, 0x0F, 0x10, 0x80, 0xE0, 0x01, 0x00, 0x00, 0x0F, 0x2E, 0xC1, 0x7A, 0x02, 0x74, 0x0F, 0xF3, 0x0F, 0x11, 0x88, 0xE0, 0x01, 0x00, 0x00, 0xC6, 0x80, 0x58, 0x01, 0x00, 0x00, 0x01, 0xC3 };
        public static readonly byte[] BaseFovSetCode = { 0xF3, 0x0F, 0x10, 0x40, 0x38, 0x48, 0x8B, 0xCB, 0x0F, 0x5A, 0xC0, 0x48, 0x85, 0xFF, 0x0F, 0x84, 0x77, 0xFC, 0xFF, 0xFF, 0x66, 0x0F, 0x5A, 0xC0, 0x48, 0x8B, 0xD6, 0xF3, 0x0F, 0x11, 0x47, 0x24 };
        public static readonly byte[] ZoomFovSetCode = { 0x66, 0x0F, 0x5A, 0xC0, 0xF3, 0x0F, 0x11, 0x47, 0x28, 0x48, 0x8B, 0x5C, 0x24, 0x30, 0x48, 0x8B, 0x6C, 0x24, 0x38, 0x48, 0x8B, 0x74, 0x24, 0x40, 0x48, 0x83, 0xC4, 0x20, 0x5F, 0xC3 };
        public static readonly byte[] VignetteOverride = { 0x48, 0x8B, 0x41, 0x30, 0xF3, 0x0F, 0x10, 0x80, 0xE0, 0x01, 0x00, 0x00, 0x0F, 0x2E, 0xC1, 0xC7, 0x80, 0xE0, 0x01, 0x00, 0x00, 0x00, 0x00, 0x80, 0x3F, 0x90, 0x90, 0xC6, 0x80, 0x58, 0x01, 0x00, 0x00, 0x01, 0xC3 };
        public static readonly byte[] BaseFovOverride = { 0xF3, 0x0F, 0x10, 0x40, 0x38, 0x48, 0x8B, 0xCB, 0x0F, 0x5A, 0xC0, 0x48, 0x85, 0xFF, 0x0F, 0x84, 0x77, 0xFC, 0xFF, 0xFF, 0x48, 0x8B, 0xD6, 0xC7, 0x47, 0x24, 0x00, 0x00, 0xD2, 0x42, 0x90, 0x90 };
        public static readonly byte[] ZoomFovOverride = { 0xC7, 0x47, 0x28, 0x00, 0x00, 0xBC, 0x42, 0x90, 0x90 };

        private static Process[] gameProcess;

        public static ulong BaseFovAddress;
        public static ulong ZoomFovAddress;
        public static ulong VignetteCodeAddress;

        [STAThread]
        public static bool GetProcessHandle(string executableName)
        {
            var notStarted = false;
            var found = false;
            while (!found)
            {
                gameProcess = Process.GetProcessesByName(executableName);
                if (gameProcess.Length < 1)
                {
                    notStarted = true;
                    Thread.Sleep(200);
                    continue;
                }

                foreach (Process proc in gameProcess)
                {
                    if (proc.MainWindowTitle.Contains("Resident Evil") || proc.MainWindowTitle.Contains("BIOHAZARD") || proc.MainWindowTitle.Contains("VILLAGE"))
                    {
                        gameProcess[0] = proc;
                        found = true;
                        break;
                    }
                }
            }

            return notStarted;
        }

        [STAThread]
        public static void SetFov(float baseFov, float zoomFov)
        {
            var customBaseFovOverride = BaseFovOverride;
            var customZoomFovOverride = ZoomFovOverride;
            var newBaseFov = BitConverter.GetBytes(baseFov);
            customBaseFovOverride[26] = newBaseFov[0];
            customBaseFovOverride[27] = newBaseFov[1];
            customBaseFovOverride[28] = newBaseFov[2];
            customBaseFovOverride[29] = newBaseFov[3];

            var newZoomFov = BitConverter.GetBytes(zoomFov);
            customZoomFovOverride[3] = newZoomFov[0];
            customZoomFovOverride[4] = newZoomFov[1];
            customZoomFovOverride[5] = newZoomFov[2];
            customZoomFovOverride[6] = newZoomFov[3];

            MemoryTools.MemoryTools.WriteSingleArray(gameProcess[0], BaseFovAddress, customBaseFovOverride);
            MemoryTools.MemoryTools.WriteSingleArray(gameProcess[0], ZoomFovAddress, customZoomFovOverride);
        }

        [STAThread]
        public static void EnableVignetteOverride()
        {
            MemoryTools.MemoryTools.WriteSingleArray(gameProcess[0], VignetteCodeAddress, VignetteOverride);
        }

        [STAThread]
        public static void DisableVignetteOverride()
        {
            MemoryTools.MemoryTools.WriteSingleArray(gameProcess[0], VignetteCodeAddress, VignetteSetCode);
        }

        [STAThread]
        public static void SetVignette(float vignette)
        {
            //var newVignette = BitConverter.GetBytes((trkVignette.Value / 100f) * -1f);
            var newVignette = BitConverter.GetBytes(vignette);
            var customVignetteOverride = VignetteOverride;
            customVignetteOverride[21] = newVignette[0];
            customVignetteOverride[22] = newVignette[1];
            customVignetteOverride[23] = newVignette[2];
            customVignetteOverride[24] = newVignette[3];
            MemoryTools.MemoryTools.WriteSingleArray(gameProcess[0], VignetteCodeAddress, customVignetteOverride);
        }

        public static void SetTargetAddresses(ulong baseFovStart, ulong baseFovEnd, ulong zoomFovStart, ulong zoomFovEnd, ulong vignetteStart, ulong vignetteEnd)
        {
            BytePatternConfig searchTargetConfig = new BytePatternConfig(BaseFovSetCode, baseFovStart, baseFovEnd, WindowsMemoryHelper.RegionPageProtection.PAGE_EXECUTE_READ);
            BytePattern searchTargetFinal = new BytePattern(searchTargetConfig);

            ulong baseAddress;
            try
            {
                baseAddress = MemoryTools.MemoryTools.GetAddressByteArray(gameProcess[0], searchTargetFinal, baseFovStart, baseFovEnd);
                if (baseAddress == 0)
                {
                    if (baseFovStart == 0UL && baseFovEnd == 0UL)
                    {
                        var error = "Search target = " + searchTargetFinal.Bytes.ToString();
                        throw new MemorySearchException();
                    }
                    baseAddress = MemoryTools.MemoryTools.GetAddressByteArray(gameProcess[0], searchTargetFinal, 0, 0);
                    if (baseAddress == 0)
                    {
                        var error = "Search target = " + searchTargetFinal.Bytes.ToString();
                        throw new MemorySearchException();
                    }
                }

                BaseFovAddress = baseAddress;

                searchTargetConfig = new BytePatternConfig(ZoomFovSetCode, zoomFovStart, zoomFovEnd, WindowsMemoryHelper.RegionPageProtection.PAGE_EXECUTE_READ);
                searchTargetFinal = new BytePattern(searchTargetConfig);

                baseAddress = MemoryTools.MemoryTools.GetAddressByteArray(gameProcess[0], searchTargetFinal, zoomFovStart, zoomFovEnd);
                if (baseAddress == 0)
                {
                    if (zoomFovStart == 0UL && zoomFovEnd == 0UL)
                    {
                        var error = "Search target = " + searchTargetFinal.Bytes.ToString();
                        throw new MemorySearchException();
                    }
                    baseAddress = MemoryTools.MemoryTools.GetAddressByteArray(gameProcess[0], searchTargetFinal, 0, 0);
                    if (baseAddress == 0)
                    {
                        var error = "Search target = " + searchTargetFinal.Bytes.ToString();
                        throw new MemorySearchException();
                    }
                }

                ZoomFovAddress = baseAddress;

                searchTargetConfig = new BytePatternConfig(VignetteSetCode, vignetteStart, vignetteEnd, WindowsMemoryHelper.RegionPageProtection.PAGE_EXECUTE_READ);
                searchTargetFinal = new BytePattern(searchTargetConfig);

                baseAddress = MemoryTools.MemoryTools.GetAddressByteArray(gameProcess[0], searchTargetFinal, vignetteStart, vignetteEnd);
                if (baseAddress == 0)
                {
                    if (vignetteStart == 0UL && vignetteEnd == 0UL)
                    {
                        var error = "Search target = " + searchTargetFinal.Bytes.ToString();
                        throw new MemorySearchException();
                    }
                    baseAddress = MemoryTools.MemoryTools.GetAddressByteArray(gameProcess[0], searchTargetFinal, 0, 0);
                    if (baseAddress == 0)
                    {
                        var error = "Search target = " + searchTargetFinal.Bytes.ToString();
                        throw new MemorySearchException();
                    }
                }

                VignetteCodeAddress = baseAddress;

                GC.Collect();
            }
            catch
            {
                throw new MemorySearchException();
            }
        }

        public static void CloseHandles()
        {
            foreach (Process proc in gameProcess)
            {
                proc.Close();
            }
        }
    }

    [Serializable]
    public class MemorySearchException : Exception
    {
        public MemorySearchException(string message) : base(message)
        {
        }

        public MemorySearchException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public MemorySearchException()
        {
        }

        protected MemorySearchException(System.Runtime.Serialization.SerializationInfo serializationInfo, System.Runtime.Serialization.StreamingContext streamingContext)
        {
            throw new NotImplementedException();
        }
    }
}

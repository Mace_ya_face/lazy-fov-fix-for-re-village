﻿using System;
using System.IO;
using Lazy_FoV_Fix_Resident_Evil_Village.Core;
using MemoryTools;

namespace Lazy_FoV_Fix_Resident_Evil_Village_HardMod_Edition
{
    public class Program
    {
        private static string ExeName = @".\re8.exe";
        public static void Main(string[] args)
        {
            Console.ForegroundColor = ConsoleColor.DarkMagenta;
            Console.WriteLine("Lazy FoV And Vignette Fix: Resident Evil Village");
            Console.WriteLine();
            Console.WriteLine();

            confirmExecutableExists();

            var baseFov = getUserFov();
            var zoomFov = getUserZoomFov();
            var vignette = getUserVignette();

            var baseFovBytes = BitConverter.GetBytes(baseFov);
            var zoomFovBytes = BitConverter.GetBytes(zoomFov);
            var vignetteBytes = BitConverter.GetBytes(vignette);

            var customBaseFovOverride = ResidentEvilTools.BaseFovOverride;
            customBaseFovOverride[26] = baseFovBytes[0];
            customBaseFovOverride[27] = baseFovBytes[1];
            customBaseFovOverride[28] = baseFovBytes[2];
            customBaseFovOverride[29] = baseFovBytes[3];

            var customZoomFovOverride = ResidentEvilTools.ZoomFovOverride;
            customZoomFovOverride[3] = zoomFovBytes[0];
            customZoomFovOverride[4] = zoomFovBytes[1];
            customZoomFovOverride[5] = zoomFovBytes[2];
            customZoomFovOverride[6] = zoomFovBytes[3];

            var customVignetteOverride = ResidentEvilTools.VignetteOverride;
            customVignetteOverride[21] = vignetteBytes[0];
            customVignetteOverride[22] = vignetteBytes[1];
            customVignetteOverride[23] = vignetteBytes[2];
            customVignetteOverride[24] = vignetteBytes[3];

            setCustomFovAndVignette(customBaseFovOverride, customZoomFovOverride, customVignetteOverride);

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Done!");

            Console.ReadLine();
        }

        private static void confirmExecutableExists()
        {
            if (!File.Exists(ExeName))
            {
                Console.ForegroundColor = ConsoleColor.DarkYellow;
                Console.WriteLine($"Can't find re8.exe. Please place this patcher next to {ExeName.Replace(@".\", "")}, or, enter it's location:");
                ExeName = Console.ReadLine();
                confirmExecutableExists();
            }
        }

        private static float getUserFov()
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("Please enter your desired base FoV:");
            var userBaseFov = Console.ReadLine();
            float userBaseFovFloat = 81f;
            try
            {
                userBaseFovFloat = float.Parse(userBaseFov);
            }
            catch
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("ERROR | Invalid FoV.");
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("Please enter your desired base FoV:");
            }

            if (userBaseFovFloat < 25)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("ERROR | Please enter an FoV above 25.");
                userBaseFovFloat = getUserFov();
            }
            else if (userBaseFovFloat > 150)
            {
                userBaseFovFloat = checkAndConfirmLargeFov(userBaseFovFloat);
            }

            return userBaseFovFloat;
        }

        private static float getUserZoomFov()
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("Please enter your desired zoom/aiming FoV:");
            var userZoomFov = Console.ReadLine();
            float userZoomFovFloat = 70f;
            try
            {
                userZoomFovFloat = float.Parse(userZoomFov);
            }
            catch
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("ERROR | Invalid FoV.");
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("Please enter your desired zoom/aiming FoV:");
            }

            if (userZoomFovFloat < 25)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("ERROR | Please enter an FoV above 25.");
                userZoomFovFloat = getUserZoomFov();
            }
            else if (userZoomFovFloat > 150)
            {
                checkAndConfirmLargeFov(userZoomFovFloat);
            }

            return userZoomFovFloat;
        }

        private static float checkAndConfirmLargeFov(float fov)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("FoV values above 150 are not stable. Would you like it set to 150 instead? (Y/N)");
            var userConfirm = Console.ReadKey();
            if (userConfirm.KeyChar.ToString().ToLower() == "y")
            {
                return 150f;
            }
            else if (userConfirm.KeyChar.ToString().ToLower() != "n")
            {
                return checkAndConfirmLargeFov(fov);
            }

            return fov;
        }

        private static float getUserVignette()
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("Vignette range is from 0 to 200.");
            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("0   | Off");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("100 | Default");
            Console.ForegroundColor = ConsoleColor.DarkRed;
            Console.WriteLine("200 | Black screen");
            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("Please enter desired vignette intensity:");
            var userZoomFov = Console.ReadLine();

            float userVignetteFloat;
            if (!float.TryParse(userZoomFov, out userVignetteFloat) || userVignetteFloat < 0 || userVignetteFloat > 200)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("ERROR | Please enter an valid vignette intensity.");
                return getUserVignette();
            }

            return ((userVignetteFloat - 100f) / 100f) * -1f;
        }

        private static void setCustomFovAndVignette(byte[] customBaseFovOverride, byte[] customZoomFovOverride, byte[] customVignetteOverride)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("Backing up original executable...");

            try
            {
                File.Move(@".\re8.exe", @".\re8.exe.backup");
            }
            catch
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"FATAL ERROR | Failed to read {ExeName}. Please ensure that {ExeName} is not open/open in another program. Please ensure you have permissions for this folder/file. Please try disabling your AV/Windows Defender. Please try running patcher as admin.");
            }

            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("Reading bytes...");

            byte[] exeBytes = null;
            try
            {
                exeBytes = File.ReadAllBytes(@".\re8.exe.backup");
            }
            catch
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"FATAL ERROR | Failed to backup {ExeName}. Please ensure that {ExeName} is not open/open in another program. Please ensure you have permissions for this folder/file. Please try disabling your AV/Windows Defender. Please try running patcher as admin.");
            }

            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("Finding functions...");

            if (exeBytes != null)
            {
                var baseFovFuncLocation = MemoryTools.MemoryTools.Locate(exeBytes, ResidentEvilTools.BaseFovSetCode);
                var newBytesCounter = 0;
                for (int i = baseFovFuncLocation; i < baseFovFuncLocation + customBaseFovOverride.Length; i++)
                {
                    exeBytes[i] = customBaseFovOverride[newBytesCounter];
                    newBytesCounter++;
                }

                var zoomFovFuncLocation = MemoryTools.MemoryTools.Locate(exeBytes, ResidentEvilTools.ZoomFovSetCode);
                newBytesCounter = 0;
                for (int i = zoomFovFuncLocation; i < zoomFovFuncLocation + customZoomFovOverride.Length; i++)
                {
                    exeBytes[i] = customZoomFovOverride[newBytesCounter];
                    newBytesCounter++;
                }

                var vignetteFuncLocation = MemoryTools.MemoryTools.Locate(exeBytes, ResidentEvilTools.VignetteSetCode);
                newBytesCounter = 0;
                for (int i = vignetteFuncLocation; i < vignetteFuncLocation + customVignetteOverride.Length; i++)
                {
                    exeBytes[i] = customVignetteOverride[newBytesCounter];
                    newBytesCounter++;
                }

                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("Writing changes...");

                try
                {
                    File.WriteAllBytes(@".\re8.exe", exeBytes);
                }
                catch
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine($"FATAL ERROR | Failed to write changes. Please ensure that {ExeName} is not open/open in another program. Please ensure you have permissions for this folder/file. Please try disabling your AV/Windows Defender. Please try running patcher as admin.");
                }

                return;
            }

            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine($"FATAL ERROR | Failed to read {ExeName}. Please ensure that {ExeName} is not open/open in another program. Please ensure you have permissions for this folder/file. Please try disabling your AV/Windows Defender. Please try running patcher as admin.");
        }
    }
}
